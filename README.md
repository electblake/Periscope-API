# Periscope API

### Version
1.4.4

Retrieve informations from periscope profiles and broadcast.
You can save both replay and live.

### Pré-requis
- NodeJs: https://nodejs.org/
- livestreamer: http://docs.livestreamer.io/install.html

### Installation
```sh
git clone https://framagit.org/Sobralia/Periscope-API.git
npm install
```

### Utilisation

```
var periscope = request('periscope-api');

periscope.getProfileInfo("userName", function(err, profile){
	console.log(profile.infos.username);
});

periscope.getBroadcastInfo("idBroadcast", function(err, broadcast){
	console.log(broadcast.broadcast.status);
});

periscope.recordBroadcast("idBroadcast", "path to file", function(err, broadcast){

});

periscope.downloadBroadcast("idBroadcast", "path to file", function(err, broadcast){

});
```

### TODO
- Fetch chat for lives and replays

### CHANGELOG

## [1.4.4]
- Changes according to the new periscope API (again)

## [1.2.4]
- Changes according to the new periscope API
